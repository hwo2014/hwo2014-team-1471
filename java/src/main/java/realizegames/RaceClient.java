/*
 * Copyright 2014 Roberto Badaro (Team: Realize Games)
 * 
 * - Was just for fun ;)
 * 
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package realizegames;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import realizegames.Bot.TrackUtil;
import realizegames.data.CarId;
import realizegames.data.CarInfo;
import realizegames.data.CarPosition;
import realizegames.data.Piece;
import realizegames.data.PieceDeserializer;
import realizegames.data.RaceInfo;
import realizegames.msg.JoinMsg;
import realizegames.msg.JoinMsg.BotId;
import realizegames.msg.JoinRaceMsg;
import realizegames.msg.JoinRaceMsg.JoinRace;
import realizegames.msg.Msg;
import realizegames.msg.PingMsg;
import realizegames.msg.ServerMsg;
//import realizegames.v2.Bot;
//import realizegames.v2.TrackUtil;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSyntaxException;

/**
 * Recebe e envia as mensagems para o servidor do jogo.
 * 
 * @author Roberto Badaro
 * 
 */
public class RaceClient /*implements RaceClientInterface */ {

    private static final String TAG = "CLIENT";

    private final Gson gson;
    private final JsonParser parser;
    private final BufferedReader reader;
    private final PrintWriter writer;
    private final BotId myBotId;


    @SuppressWarnings("serial")
    private final Type carPositionsArrayType = new TypeToken<CarPosition[]>() {
    }.getType();

    private Bot bot;
    private RaceInfo raceInfo;
    private List<Integer> retao = null;

    public RaceClient(final BufferedReader reader, final PrintWriter writer, final BotId myBotId) {
        this.reader = reader;
        this.writer = writer;
        this.myBotId = myBotId;
        gson = new GsonBuilder().registerTypeAdapter(Piece.class, new PieceDeserializer()).create();
        parser = new JsonParser();
    }

    public void join() throws IOException {
        Logger.info(TAG, "Sending join");
        sendMsg(new JoinMsg(myBotId));
    }
    
    public void joinRace(final String pista, final int carcount) throws IOException {
        if ("1".equals(System.getProperty("rzgames-dev", "0")) || carcount > 1) {
            Logger.info(TAG, "Sending joinRace");
            sendMsg(new JoinRaceMsg(new JoinRace(myBotId, pista, carcount)));
        } else {
            join();
        }
    }

    public void listen() throws IOException {
        String line = null;
        while ((line = reader.readLine()) != null) {
            try {
                final JsonObject json = parser.parse(line).getAsJsonObject();
                final ServerMsg msgFromServer = serverMsg(json);
                serverLoop(msgFromServer, json, line);
            } catch (final JsonSyntaxException e) {
                // Envia p/ log.
                System.out.println(e);
            }
        }

        Logger.info(TAG, "Fim de comunicacao.");
        Logger.info(TAG, "BOT - topSpeed: " + bot.topSpeed + "; maxAc: " + bot.maxAc + "; maxDesac: " +
                bot.maxDeac + "; gMax: " + bot.maxAcAng);
    }

    protected void sendMsg(final Msg msg) {
        writer.println(gson.toJson(msg));
        writer.flush();
    }

    private boolean joined;
    private final PingMsg ping = new PingMsg();
    boolean ontrack = false;

    protected void serverLoop(final ServerMsg msgFromServer, final JsonObject json, final String line) {
        switch (msgFromServer.msgType) {
        case "carPositions":
            drive(msgFromServer, json);
            break;
        case "turboAvailable":
            ping();
            turboAvailable(msgFromServer, json);
            break;
        case "join":
        case "joinRace":
            joined = true;
            ping();
            Logger.info(TAG, "Joined - %s", line);
            break;
        case "gameInit":
            ping();
            Logger.info(TAG, "Game init\n%s", line);
            configRace(json);
            break;
        case "gameEnd":
            ping();
            ontrack = false;
            Logger.info(TAG, "Race end\n %s", line);
            break;
        case "gameStart":
            ontrack = true;
            sendMsg(bot.prepareForStart());
            Logger.info(TAG, "Race start\n%s", line);
            break;
        case "yourCar":
            ping();
            myCar(json);
            Logger.info(TAG, "yourCar\n%s", line);
            break;
        case "crash":
            ping();
            crashCheck(msgFromServer, json);
            Logger.info(TAG, line);
            break;
        case "spawn":
            ping();
            spawnCheck(msgFromServer, json);
            Logger.info(TAG, line);
            break;
        case "dnf":
            ping();
            Logger.info(TAG, line);
            break;
        case "finish":
            ping();
            finishCheck(msgFromServer, json);
            Logger.info(TAG, line);
            break;
        case "lapFinished":
            Logger.info(TAG, line);
            break;
        case "tournamentEnd":
            ontrack = false;
            Logger.info(TAG, line);
            break;
        case "error":
            ontrack = false;
            Logger.error(TAG, line);
            if (!joined) {
                Logger.error(TAG, "SERVIDOR RETORNOU ERRO. ABORTANDO EXECUCAO.");
                return;
            }
        default:
            ping();
            Logger.warn(TAG, "qqisso? %s", line);
            break;
        }
    }
    
    private void ping() {
        sendMsg(ping);
    }

    private void crashCheck(final ServerMsg msgFromServer, final JsonObject json) {
        if (isMyCar(json)) {
            bot.crash();
            ontrack = false;
        }
    }

    private void spawnCheck(final ServerMsg msgFromServer, final JsonObject json) {
        if (isMyCar(json)) {
            bot.spawn();
            ontrack = true;
        }
    }

    private void finishCheck(final ServerMsg msgFromServer, final JsonObject json) {
        if (isMyCar(json)) {
            ontrack = false;
            Logger.info(TAG, "WE FINESHED!");
        }
    }

    private boolean isMyCar(final JsonObject json) {
        final String color = json.get("data").getAsJsonObject().getAsJsonPrimitive("color").getAsString();
        return bot.id.color.equals(color);
    }

    /**
     * @param json
     */
    private void myCar(final JsonObject json) {
        final CarId carId = gson.fromJson(json.get("data"), CarId.class);
        bot = new Bot(carId);
    }

    /**
     * @param json
     */
    private void configRace(final JsonObject json) {
        raceInfo = gson.fromJson(json.get("data"), RaceInfo.class);
        for (final CarInfo info : raceInfo.race.cars) {
            if (info.id.color.equals(bot.id.color)) {
                bot.myCar(info);
                break;
            }
        }
        
        TrackUtil.calculateThresholds(raceInfo.race.track.pieces, bot.mycar);
        identificarRetao();
    }
    
    private void identificarRetao() {
        final List<List<Integer>> retas = new ArrayList<>();
        List<Integer> reta = new ArrayList<>();
        final Piece[] pieces = raceInfo.race.track.pieces;
        for (int i = 0; i < pieces.length; i++) {
            final Piece p = pieces[i];
            if (!p.isBend() /*|| p.radius >= 200*/) {
                reta.add(i);
            } else {
                if (!reta.isEmpty()) {
                    retas.add(reta);
                    reta = new ArrayList<>();
                }
            }
        }
        if (!reta.isEmpty()) {
            retas.add(reta);
            reta = new ArrayList<>();
        }
        if (!retas.isEmpty() && retas.size() > 1) {
            final List<Integer> reta1 = retas.get(0);
            if (reta1.get(0) == 0) {
                final List<Integer> ultReta = retas.get(retas.size() -1);
                if (ultReta.get(ultReta.size() -1) == pieces.length -1) {
                    retas.remove(retas.size() -1);
                    reta1.addAll(0, ultReta);
                }
            }
        }
        
        int maior = 0;
        final int numRetas = retas.size();
        for (int i =0; i<numRetas;i++) {
            final List<Integer> umaReta = retas.get(i);
            if (umaReta.size() > maior) {
                maior = umaReta.size();
                retao = umaReta;
            }
        }
        
        bot.retao = retao;
    }

    /**
     * @param json
     */
    private void drive(final ServerMsg msgFromServer, final JsonObject json) {
        if (!ontrack) {
            ping();
            bot.stop();
            return;
        }

        final CarPosition[] positions = gson.fromJson(json.get("data"), carPositionsArrayType);
        final CarId mycarId = bot.id;
        for (final CarPosition pos : positions) {
            if (pos.id.color.equals(mycarId.color)) {
                sendMsg(bot.nextCommand(pos, positions, raceInfo.race.track.pieces,
                    raceInfo.race.track.lanes, msgFromServer.gameTick));
                return;
            }
        }
        // nao deve chegar aqui...
        ping();
    }

    private void turboAvailable(final ServerMsg msgFromServer, final JsonObject json) {
        final JsonObject jo = json.get("data").getAsJsonObject();
        final double factor = jo.getAsJsonPrimitive("turboFactor").getAsDouble();
        final int duration = jo.getAsJsonPrimitive("turboDurationTicks").getAsInt();
        bot.turboAvailable(factor, duration);
    }

    protected ServerMsg serverMsg(final JsonObject msg) {
        final ServerMsg from = new ServerMsg();
        try {
            from.msgType = msg.getAsJsonPrimitive("msgType").getAsString();
            JsonPrimitive primitive = null;
            if ((primitive = msg.getAsJsonPrimitive("gameId")) != null) {
                from.gameId = primitive.getAsString();
            }
            if ((primitive = msg.getAsJsonPrimitive("gameTick")) != null) {
                from.gameTick = primitive.getAsInt();
            }
            return from;
        } catch (final Exception e) {
            from.msgType = "corrupted_" + from.msgType;
            return from;
        }
    }

}
