/*
 * Copyright 2014 Roberto Badaro (Team: Realize Games)
 * 
 * - Was just for fun ;)
 * 
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package realizegames;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Arrays;

import realizegames.msg.JoinMsg.BotId;

public class Main {

    private static final String TAG = "MAIN";

    public static void main(final String... args) {
        
        System.out.println("Main args: " + Arrays.toString(args));
        
        final String host = args[0];
        final int port = Integer.parseInt(args[1]);
        final String botName = args[2];
        final String botKey = args[3];
        String pista = ""; // default

        boolean multibot = false;
        int carcount = 1;
        
        if (args.length > 4) {
            pista = args[4];
        }
        if (args.length > 5) {
            multibot = true;
            carcount = Integer.valueOf(args[5]);
        }

        if ("1".equals(System.getProperty("rzgames-dev", "0")) &&
            "1".equals(System.getProperty("rzgames-debug", "0"))) {
            Logger.loggerOn(Logger.Level.DEBUG);
        } else {
            Logger.loggerOn(Logger.Level.INFO);
        }

        Logger.info(TAG, "Connecting to " + host + ":" + port + " as " + botName);
        Logger.debug(TAG, "Connecting to " + host + ":" + port + " as " + botName + "/" + botKey + " - " +
                pista);

        try (final Socket socket = new Socket(host, port);) {
            
            final PrintWriter writer =
                new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
            
            final BufferedReader reader =
                new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

            RaceClient client = null;
//            if (botName.startsWith("Trafic")) {
//                client = new RaceClient2(reader, writer, new BotId(botName, botKey));
//            } else {
                client = new RaceClient(reader, writer, new BotId(botName, botKey));
//            }
            
            if (System.getProperty("rzgames-dev") == null && !multibot) {
                client.join();
            } else {
                client.joinRace(pista, carcount);
            }
            client.listen();
            Logger.info(TAG, "Saida normal.");
        } catch (final Exception e) {
            Logger.error(TAG, e, "Saida por erro.");
        }
    }

}
