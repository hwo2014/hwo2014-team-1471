/*
 * Copyright 2014 Roberto Badaro (Team: Realize Games)
 * 
 * - Was just for fun ;)
 * 
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package realizegames.msg;


public class SwitchLaneMsg extends FullMsg {
    
    private static enum Singleton {
        INSTANCE;
        private SwitchLaneMsg msg = new SwitchLaneMsg(null, null);
        SwitchLaneMsg get() {
            return msg;
        }
    }
    
    public static SwitchLaneMsg getMsg(final String direction, final Integer gameTick) {
        final SwitchLaneMsg msg = Singleton.INSTANCE.get();
        msg.data = direction;
        msg.gameTick = gameTick;
        return msg;
    }
    
    public static SwitchLaneMsg left(final Integer gameTick) {
        return getMsg("Left", gameTick);
    }
    public static SwitchLaneMsg right(final int gameTick) {
        return getMsg("Right", gameTick);
    }

    private SwitchLaneMsg(final String direction, final Integer gameTick) {
        super("switchLane", direction, gameTick);
    }
}