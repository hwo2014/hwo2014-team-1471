/*
 * Copyright 2014 Roberto Badaro (Team: Realize Games)
 * 
 * - Was just for fun ;)
 * 
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package realizegames.msg;


public class ThrottleMsg extends FullMsg {
    
    private static enum Singleton {
        INSTANCE;
        private ThrottleMsg msg = new ThrottleMsg(0, null);
        ThrottleMsg get() {
            return msg;
        }
    }
    
    public static ThrottleMsg getMsg(final double howmuch, final Integer gameTick) {
        final ThrottleMsg msg = Singleton.INSTANCE.get();
        msg.data = Double.valueOf(howmuch);
        msg.gameTick = gameTick;
        return msg;
    }
    
    
    private ThrottleMsg(final double howmuch, final Integer gameTick) {
        super("throttle", howmuch, gameTick);
    }
}