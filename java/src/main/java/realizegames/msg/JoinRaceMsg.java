/*
 * Copyright 2014 Roberto Badaro (Team: Realize Games)
 * 
 * - Was just for fun ;)
 * 
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package realizegames.msg;

import realizegames.msg.JoinMsg.BotId;

public class JoinRaceMsg extends Msg {

    public JoinRaceMsg(final JoinRace createRace) {
        super("joinRace", createRace);
    }

    public static class JoinRace {
        public BotId botId;
        public String trackName;
        public String password;
        public Integer carCount;

        public JoinRace() {
        }

        public JoinRace(final BotId botId) {
            this(botId, null, null, null);
        }
        
        public JoinRace(final BotId botId, final int carCount) {
            this(botId, null, null, carCount);
        }

        public JoinRace(final BotId botId, final String trackName, final int carCount) {
            this(botId, trackName, (carCount <= 2 ? null : "rzg2014hwo"), carCount);
        }

        public JoinRace(final BotId botId, final String trackName, final String password, final Integer carCount) {
            this.botId = botId;
            this.trackName = trackName;
            this.password = password;
            this.carCount = carCount;
        }
    }
}
