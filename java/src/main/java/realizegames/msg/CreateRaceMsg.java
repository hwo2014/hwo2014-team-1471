/*
 * Copyright 2014 Roberto Badaro (Team: Realize Games)
 * 
 * - Was just for fun ;)
 * 
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package realizegames.msg;

import realizegames.msg.JoinMsg.BotId;

// create or join, pois o joinrace e igual, so muda o msgtype.
public class CreateRaceMsg extends Msg {

    public CreateRaceMsg(final CreateRace createRace) {
        super("createRace", createRace);
    }

    public static class CreateRace {
        public BotId botId;
        public String trackName;
        public String password;
        public int carCount;
        public CreateRace() {
        }

        public CreateRace(final BotId botId, final int carCount) {
            this(botId, null, null, carCount);
        }
        public CreateRace(final BotId botId, final String trackName, final int carCount) {
            this(botId, trackName, null, carCount);
        }
        public CreateRace(final BotId botId, final String trackName, final String password, final int carCount) {
            this.botId = botId;
            this.trackName = trackName;
            this.password = password;
            this.carCount = carCount;
        }
    }
}
