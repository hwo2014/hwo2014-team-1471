/*
 * Copyright 2014 Roberto Badaro (Team: Realize Games)
 * 
 * - Was just for fun ;)
 * 
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package realizegames.data;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;

/**
 * So por causa da palvra reservada "switch"...
 */
public class PieceDeserializer implements JsonDeserializer<Piece> {
    @Override
    public Piece deserialize(final JsonElement json,
                             final Type typeOfT,
                             final JsonDeserializationContext context) throws JsonParseException {

        final JsonObject jo = json.getAsJsonObject();
        final Piece piece = new Piece();
        JsonPrimitive primitive = null;

        if ((primitive = jo.getAsJsonPrimitive("length")) != null) {
            piece.length = primitive.getAsDouble();
        }
        if ((primitive = jo.getAsJsonPrimitive("radius")) != null) {
            piece.radius = primitive.getAsDouble();
        }
        if ((primitive = jo.getAsJsonPrimitive("angle")) != null) {
            piece.angle = primitive.getAsDouble();
        }
        if ((primitive = jo.getAsJsonPrimitive("switch")) != null) {
            piece.suitch = primitive.getAsBoolean();
        }

        calcBendLen(piece);
        return piece;
    }

    void calcBendLen(final Piece piece) {
        if (piece.isBend()) {
            piece.length = (Math.abs(piece.angle) * Math.PI * piece.radius) / 180d;
        }
    }
}
