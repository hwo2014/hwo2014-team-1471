/*
 * Copyright 2014 Roberto Badaro (Team: Realize Games)
 * 
 * - Was just for fun ;)
 * 
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package realizegames;

/**
 * Simple stdout logger.
 * 
 * @author Roberto Badaro
 */
public class Logger {

    // tag, level, msg, throwable
    private static final String MSG_WITH_EXCEPTION = "%s + [%s] %s (%s)";
    // tag, level, msg
    private static final String MSG_WITH_ARGS = "%s + [%s] %s";

    private static int permitedLevel;

    public static void loggerOn() {
        loggerOn(Level.ALL);
    }

    public static void loggerOn(final Level level) {
        if (level != null) {
            permitedLevel = level.ordinal();
        }
    }

    public static void loggerOff() {
        permitedLevel = Level.NONE.ordinal();
    }

    public static boolean isDebugEnabled() {
        return Level.DEBUG.ordinal() <= permitedLevel;
    }

    public static void debug(final Object tag, final String msg, final Object... args) {
        log(tag, Level.DEBUG, msg, args);
    }

    public static void debug(final Object tag, final Throwable throwable, final String msg) {
        log(tag, Level.DEBUG, msg, throwable);
    }

    public static void info(final Object tag, final String msg, final Object... args) {
        log(tag, Level.INFO, msg, args);
    }

    public static void info(final Object tag, final Throwable throwable, final String msg) {
        log(tag, Level.INFO, msg, throwable);
    }

    public static void warn(final Object tag, final String msg, final Object... args) {
        log(tag, Level.WARN, msg, args);
    }

    public static void warn(final Object tag, final Throwable throwable, final String msg) {
        log(tag, Level.WARN, msg, throwable);
    }

    public static void error(final Object tag, final String msg, final Object... args) {
        log(tag, Level.ERROR, msg, args);
    }

    public static void error(final Object tag, final Throwable throwable, final String msg) {
        log(tag, Level.ERROR, msg, throwable);
    }

    private static void log(final Object tag, final Level level, final String msg, final Throwable throwable) {
        if (level.ordinal() <= permitedLevel) {
            System.out.println(String.format(MSG_WITH_EXCEPTION, tag, level, msg, throwable));
            if (throwable != null) {
                throwable.printStackTrace(System.out);
            }
        }
    }

    private static void log(final Object tag, final Level level, final String msg, final Object... args) {
        if (level.ordinal() <= permitedLevel) {
            final String basemsg = String.format(MSG_WITH_ARGS, tag, level, msg);
            System.out.println(String.format(basemsg, args));
        }
    }

    public static enum Level {
        NONE, ERROR, WARN, INFO, DEBUG, ALL
    }

}
